/**
 * Created by pc1 on 27/05/15.
 */
dashboardControllers.controller('dashboardCtrl',[ '$scope', '$firebase', 'URL','isDev', 'Auth', '$filter', '$location','Push','$http',
    function($scope, $firebase, URL,isDev, Auth, $filter,  $location,Push,$http) {
        //check if user is't authorizated, redirect him on login page
        Auth.signedIn();
        $scope.USERS_PER_PAGE=5;
        $scope.POSTS_PER_PAGE=50;
		$scope.isDev=isDev;
        //Menu buttons
        $scope.Logout = function() {
            Auth.logout();
        };
        $scope.moveToPushSender=function(){
            $location.path('/pushSender');
        }
        $scope.sendPush=function(message,target){
            Push.notifySend(message,target);
            $scope.toggleModal(0);
        };


        //users management
        $scope.users_current_page=0;
        $scope.lastSearch_query="";
        $scope.lastSearch_searchState="";
        $scope.server_users_count=0;
        $scope.updateUsersCache=function() {
            $.post("php/user.php",{dev:isDev,token:"kaizen",action:"getData",pre:$scope.query,is_teacher:!$scope.searchState}).success(function(raw_data, status, headers, config) {
                $scope.users = new Array();
                var data = JSON.parse(raw_data);
                for (var user in data.users){
                    data.users[user].$id=user;
                    $scope.users.push(data.users[user]);
                }
                $scope.$apply();
            });
        }
        $scope.getUsersPageCount=function(){
            if($scope.users==null){
                return "loading";
            }
            return Math.ceil($scope.users.length/$scope.USERS_PER_PAGE);
        }
        $scope.updateUsersCache();

        $scope.updateUser = function(user) {
            var usersDB = new Firebase(URL + "/users/"+user.$id);
            delete user.$id;
            delete user.$$hashKey
            user.update_date=new Date().getTime();
            usersDB.update(user)
            $scope.toggleModal(0);
        };
        $scope.searchState = false;
        $scope.showSearch = function() {
            $scope.searchState = !$scope.searchState;
            $scope.updateUsersCache();
        }

        $scope.showModal = false;
        $scope.currentUser = 0;//0-hide modal, -1 select tutor modal, else edit tutor and it's users id
        $scope.toggleModal = function(user_id) {
            $scope.currentUser = user_id;
            if (user_id === -1) {
                $scope.user = {};
            } else {
                $scope.users.forEach(function(value, key) {
                    if(value.id === user_id) {
                        $scope.user = $scope.users[key];
                    }
                });
            }
            $scope.showModal = $scope.currentUser!=0;
        };


        //managing posts
        var ref_posts = new Firebase(URL + "/posts");
        var sync_posts = $firebase(ref_posts);

        $scope.posts = sync_posts.$asArray();

        //mark all posts on 3 category
        $scope.posts.$loaded().then(function() {
            angular.forEach($scope.posts, function(value, key) {
                angular.forEach($scope.users, function(val, k) {
                    if (value.user == val.id) {
                        value.username = val.name;
                    }
                });
            });
        });



        //Work with posts
        $scope.post_current_page=0;
        //1- Unassigned Questions, 2- Assigned & Pending 3- Answered Questions
        $scope.post_filter=1;
        $scope.lastSearch_query_post_filter=1;
        $scope.updatePostCache=function(post_filter,page) {//page-0 move to page 0, page 1 move to next page, page -1 move to previos page
            if(page==0){
                $scope.post_current_page=0
            }else{
                $scope.post_current_page+=page;
                post_filter= $scope.lastSearch_query_post_filter;
            }
            $(".nav.nav-tabs li").removeClass("active");
            $(".nav.nav-tabs li:eq("+(post_filter-1)+")").addClass("active")

            var postDB = new Firebase(URL + "/posts");

            //var runSearch = ($scope.lastSearch_query_post_filter!=post_filter);
            //if (runSearch) {
                if(post_filter==1){
                    postDB=postDB.orderByChild("status").equalTo("unanswered")
                }else if(post_filter==2){
                    postDB=postDB.orderByChild("status").equalTo("assigned")
                }else if(post_filter==3){
                    postDB=postDB.orderByChild("status").equalTo("answered")
                }
                $scope.lastSearch_query_post_filter=post_filter;
            //}
            if($scope.post_current_page>0) {
            //    postDB = postDB.startAt($scope.post_current_page * $scope.POSTS_PER_PAGE)
            }

            postDB=postDB.limitToFirst($scope.POSTS_PER_PAGE);
            postDB.on("value", function (raw_data) {
                $scope.posts = new Array();
                var data = raw_data.val();
                for (var post in data) {
                    data[post].$id=post;
                    $scope.posts.push(data[post]);
                }
            });
        }
        $scope.getPostPageCount=function(){
            if($scope.posts==null){
                return "loading";
            }
            return ""+Math.ceil($scope.posts.length/$scope.POSTS_PER_PAGE);
        }
        $scope.updatePostCache(1,0);


        $scope.removeTutorFromPost=function(post,tutor){

            var postDB = new Firebase(URL + "/posts/"+post.$id);
/*
            for(var i=0;i<post.potential_tutors.length;++i){
                if(post.potential_tutors[i].id==tutor.id){
                   post.potential_tutors.splice(i, 1);
                    --i;
                }else{
                    delete post.potential_tutors[i].$$hashKey;
                }
            }*/
            delete post.potential_tutors[tutor.uid]
            delete post.$id;
            delete post.$$hashKey;
            if(Object.keys(post.potential_tutors).length==0){//no tutors selected
                post.status="unanswered";
            }
            post.update_date=new Date().getTime();
            postDB.set(post)
        }
        //<autocomplete ng-model="selectedTutorForPost" data="tutors_names" on-type="updateTutorsCache" on-select="setSelectedTutorTo(post)"></autocomplete>
        $scope.selectedTutorForPost="";
        $scope.tutors_names;//=["aaaaaaa","aa","aaaaabc"];
        $scope.tutors_names_cache;//same as names but with full tutor not just name
        $scope.selectedPost=null;
        $scope.selectPostTutor=function(post){
            $scope.selectedPost=post;

            $scope.showModal=post!=null;
            $scope.searchParam="";
            $scope.selectedTutorForPost="";

/*            setTimeout(function(){

                angular.element("autocomplete,input").val("");
            },100);
            $scope.$apply();*/
        }
        $scope.updateTutorsCache=function(query){
            $.post("php/user.php",{dev:isDev,token:"kaizen",action:"getData",pre:query,is_teacher:true}).success(function(raw_data, status, headers, config) {
                var data = JSON.parse(raw_data);
                $scope.tutors_names = new Array();
                $scope.tutors_names_cache = new Object();

                for (var user in data.users) {
                    $scope.tutors_names.push(data.users[user].name)
                    $scope.tutors_names_cache[user] = data.users[user];
                }
            });
        }
        $scope.setSelectedTutorTo=function(selected) {
            var tutor=selected.originalObject;
            var tutorId=tutor.user_id;
	        $.post("php/post.php",{dev:isDev,token:"kaizen",action:"addPostTutor",post_id:$scope.selectedPost.$id,user_id:tutorId}).success(function(raw_data, status, headers, config) {
		        $scope.selectedTutorForPost="";
		        $scope.toggleModal(0);
	        });
        }


        $scope.markAnswer = function(id, type) {
            $scope.posts.forEach(function(val, key) {
                if (val.id == id ) {
                    $scope.posts[key].status = 'answered';
                    $scope.posts[key].potential_tutors = null;
                    $scope.posts.$save(key);
                }
            });
        };

        $scope.reopen = function(id) {
            $scope.posts.forEach(function(val, key) {
                if (val.id == id ) {
                    $scope.posts[key].status = 'unanswered';
                    $scope.posts[key].potential_tutors = null;
                    $scope.posts[key].last_tutor_id = '';
                    $scope.posts.$save(key);
                }
            });
        };

        $scope.deletePost = function(id) {
            $scope.posts.forEach(function(val, key) {
                if (val.id == id ) {
                    $scope.posts.$remove(key);
                }
            });
        };

        $scope.selectedTutors = [];
        $scope.post_id = false;

        /**
         * When select tutor window is closing
         * If there is was several tutors, but we clean all => reopens post as unassigned
         * If no tutors was selected - we make next check: if not all available tutors were selected we add remaining tutors
         */
        $scope.fClose = function() {
            if ($scope.post_id !== false) {
                $scope.posts.forEach(function( val, k) {
                    if (val.id == $scope.post_id ) {
                        var i = 0;
                        var result = [];
                        $scope.selectedTutors.forEach(function(value, key) {
                            if(value.ticked === true) {
                                result.push(value);
                            } else {
                                i++;
                            }
                        });
                        //if clear all tutors in one time
                        if($scope.selectedTutors.length === i && $scope.posts[k].potential_tutors != undefined && i === $scope.posts[k].potential_tutors.length) {
                            $scope.fReset($scope.post_id);
                        }
                        //if clear tutors by turns (closing popup each time)
                        else if (result.length == 0 && $scope.posts[k].potential_tutors != undefined) {
                            i = 0;
                            $scope.posts[k].potential_tutors.forEach(function(value, key) {
                                $scope.selectedTutors.forEach(function(v, num) {
                                    if(value.id == v.id) {
                                        value.ticked = false;
                                    }
                                });
                            });
                            $scope.posts[k].potential_tutors.forEach(function(value, key) {
                                if(value.ticked == false || value.ticked == undefined) i++;
                            });
                            if ($scope.posts[k].potential_tutors.length == i) {
                                $scope.fReset($scope.post_id);
                            }
                            $scope.posts.$save(k);
                        }
                        //adding tutor
                        else if (result.length > 0){
                            if ($scope.posts[k].potential_tutors == undefined) {
                                //calculating a difference between tutors and selected teachers and add this difference
                                var dif = [];
                                $scope.tutors.forEach(function(value, key) {
                                    i = 0;
                                    result.forEach(function(v, n) {
                                        if(value.id != v.id) {
                                            i++;
                                        }
                                    });
                                    if(i == result.length) {
                                        dif.push(value);
                                    }
                                });
                                //sending notifications
                                result.forEach(function(value, key) {
                                    $scope.notifySend('New question was assigned to you', value.amazon_endpoint_arn);
                                });
                                //adding non-selected tutors to array of potential tutors
                                result = result.concat(dif);
                                $scope.posts[k].potential_tutors = result;

                                $scope.posts[k].status = 'assigned';
                            } else {
                                $scope.posts[k].potential_tutors.forEach(function(value, key) {
                                    $scope.selectedTutors.forEach(function(v, n) {
                                        if(value.id == v.id) {
                                            if(value.ticked != true && v.ticked == true) {
                                                $scope.notifySend('New question was assigned to you', value.amazon_endpoint_arn);
                                            }
                                            value.ticked = v.ticked;
                                        }
                                    });
                                });
                            }

                            $scope.posts.$save(k);
                        }
                        $scope.selectedTutors = [];
                        return;
                    }
                });
            }
        };

        $scope.fClick = function( data, id ) {
            var number;
            $scope.post_id = id;
            if ($scope.selectedTutors.length > 0) {
                $scope.selectedTutors.forEach(function(value,key) {
                    if (value.id == data.id && value.ticked != data.ticked) {
                        value.ticked = data.ticked;
                        number = key;
                    }
                });
                if (typeof number == 'undefined') {
                    $scope.selectedTutors.push(data);
                }
            } else {
                $scope.selectedTutors.push(data);
            }
        };

        $scope.fSelectAll = function(id) {
            var tutors = angular.copy($scope.tutors);
            angular.forEach(tutors, function(value, key) {
                value.ticked = true;
                $scope.notifySend('New question was assigned to you', value.amazon_endpoint_arn);
            });
            $scope.posts.forEach(function(value, key) {
                if(value.id == id) {
                    if ($scope.posts[key].status == 'unanswered') {
                        $scope.posts[key].status = 'assigned';
                    }
                    $scope.posts[key].potential_tutors = tutors;
                    $scope.posts.$save(key);
                }
            });
        };

        $scope.fReset = function(id) {
            var tutors = angular.copy($scope.tutors);
            $scope.post_id = id;
            angular.forEach(tutors, function(value, key) {
                value.ticked = false;
            });
            $scope.selectedTutors = [];
            $scope.posts.forEach(function(value, key) {
                if(value.id == id) {
                    if ($scope.posts[key].status == 'assigned') {
                        $scope.posts[key].status = 'unanswered';
                    }
                    $scope.posts[key].potential_tutors = [];
                    $scope.posts.$save(key);
                }
            });

        };

    }])

    .directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="toggleModal(-1)">&times;</button>' +
            '<h4 class="modal-title">{{ title }}</h4>' +
            '</div>' +
            '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
            restrict: 'E',
            transclude: true,
            replace:true,
            scope:true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function(value){
                    if(value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
