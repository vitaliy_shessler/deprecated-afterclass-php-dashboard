'use strict';

var authServices = angular.module('authServices',[]).
    //constant('URL', 'https://afterclass-dashboard.firebaseio.com/');
    constant('URL', 'https://dazzling-heat-8303.firebaseio.com/');

authServices.factory('Auth', ['$firebase', '$firebaseAuth', 'URL', '$location',
    function($firebase, $firebaseAuth, URL, $location){
    var ref = new Firebase(URL);

    function logout() {
        ref.unauth();
        $location.path('/signin');
    }
    function signedIn() {
        if (window.location.hash == '#/signin') {
            if (ref.getAuth() !== null) {
                $location.path('/main');
            }
        } else {
            if (ref.getAuth() === null) {
                $location.path('/signin');
            }
        }
    }

    return {
        logout: logout,
        signedIn: signedIn
    };
}]);

//amazon SNS for sending notifications
var pushService = angular.module('communicationSevices',[]).
    //constant('URL', 'https://afterclass-dashboard.firebaseio.com/');
    constant('URL', 'https://dazzling-heat-8303.firebaseio.com/');

pushService.factory('Push', ['$firebase', '$firebaseAuth', 'URL', '$location',
    function($firebase, $firebaseAuth, URL, $location){
        var sns = new AWS.SNS({
            region: 'us-west-2',
            accessKeyId: 'AKIAIWZPMXE5FSUW7N6A',
            secretAccessKey: '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'
        });



        function notifySend(message, id) {
            var params = {
                MessageStructure: 'json',
                Message: JSON.stringify({
                    "default": message,
                    "GCM": "{ \"data\": { \"message\": \"" + message + "\" }}"

                }),
                TargetArn: id
            };
            sns.publish(params, function(err,data){
                if (err) {
                    console.log('Error sending a message', err);
                } else {
                    console.log('Sent message:', data);
                }
            });
        }

        return {
            notifySend: notifySend
        };
    }
]);