'use strict';

/* Controllers */

var dashboardControllers = angular.module('dashboardControllers', []);

dashboardControllers.controller('authCtrl',[ '$scope', 'Auth', '$firebase', '$firebaseAuth', 'URL','isDev', '$location',
    function($scope, Auth, $firebase, $firebaseAuth, URL,isDev, $location) {
        Auth.signedIn();
		$scope.isDev=isDev;
        $scope.signIn = function(user) {
            var ref = new Firebase(URL);
            var loginObj = $firebaseAuth(ref);
            var username = user.email;
            var password = user.password;

            loginObj.$authWithPassword({
                email: username,
                password: password
            }).then(function(user) {
                //Success callback
                $location.path('/main');
            }, function(error) {
                //Failure callback
                $scope.showError = 1;
                $scope.error = "Check your email and password and try again";
            });
        }
    }]);
dashboardControllers.controller('pushSender',[ '$scope', 'Auth', '$firebase', '$firebaseAuth', 'URL', '$location',
    function($scope, Auth, $firebase, $firebaseAuth, URL, $location) {
        //alert("sending a push");
        //connection to DB

        $scope.searchFields=new Array();
        $scope.foundSearchFields=new Array();
        $scope.addToSearchFields=function(obj,path) {
            if (obj == null) {
                return;
            }
            if (typeof(obj) == "object") {
                for (var type in obj) {
                    $scope.addToSearchFields(obj[type],path+"."+type);
                }
            }else if(typeof(obj)=="string" ||typeof(obj)=="number"||typeof(obj)=="boolean"){
                //first make sure i won't get the same value twice
                var field={  "name":path.substring(1),
                    "value":"",
                    "type":typeof(obj)
                };
                if($scope.foundSearchFields.indexOf(path) == -1) {
                    $scope.foundSearchFields.push(path);
                    $scope.searchFields.push(field);
                }
            }else{
                alert(typeof(obj));
            }
        };
        $scope.scanForUsers=function(){
            $scope.users = new Firebase(URL + "/users");

            for(var i in $scope.searchFields){
                var field=$scope.searchFields[i];

                console.log(field.name+" = "+field.value);
                if(field.value !=""){
                    console.log("updating");
                    $scope.users=$scope.users.orderByChild(field.name);
                    if(field.type=="string") {
                        $scope.users = $scope.users.equalTo(field.value);
                    }else if(field.type=="number") {
                        $scope.users = $scope.users.equalTo(parseInt(field.value));
                    }else if(field.type=="boolean") {
                        $scope.users = $scope.users.equalTo(field.value=="true");
                    }
                }

            }
            $scope.selectedUsers=new Array();
            $scope.users.on("value", function(users) {
                $scope.selectedUsers=new Array();
                users=users.val()
                for (var user in users) {
                    $scope.selectedUsers.push(users[user]);
                }
                $scope.$apply();
            });
          }
        $scope.sendPush=function(){
            for(var user in $scope.selectedUsers){
                console.log("pushing "+$scope.message+" to "+$scope.selectedUsers[user].amazon_endpoint_arn);
                $scope.notifySend($scope.message,$scope.selectedUsers[user].amazon_endpoint_arn)
            }
        }



        //this is a duplication of code from the main controller, need to place it some where more global
        var sns = new AWS.SNS({
            region: 'us-west-2',
            accessKeyId: 'AKIAIEAG6S2HZVPZOAHQ',
            secretAccessKey: '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'
        });

        //function for sending a notifications
        $scope.notifySend = function(message, id) {
            var params = {
                MessageStructure: 'json',
                Message: JSON.stringify({
                    "default": message,
                    "GCM": "{ \"data\": { \"message\": \"" + message + "\" }}"

                }),
                TargetArn: id
            };
            sns.publish(params, function(err,data){
                if (err) {
                    console.log('Error sending a message', err);
                } else {
                    console.log('Sent message:', data);
                }
            });
        };

        var ref = new Firebase(URL + "/users");
        ref.limitToFirst(10).on("value", function(snapshot) {
            var users=snapshot.val();
            for (var user in users) {
                $scope.addToSearchFields(users[user], "");

            }
            $scope.$apply();
        });
    }]);
