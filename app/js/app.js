'use strict';

/* App Module */
var isDev=location.href.indexOf("dev")>location.href.indexOf("#") &&location.href.indexOf("#")>0
var dashboardApp = angular.module('dashboardApp', [
    'ngRoute',
    'firebase',
    'isteven-multi-select',
    'dashboardControllers',
    'authServices',
    'communicationSevices',
    'dashboard.filters',
    'angularUtils.directives.dirPagination',
    'angucomplete-alt'
]).
//constant('URL', 'https://afterclass-dashboard.firebaseio.com/');
constant('URL',"https://"+(isDev?'spankin-butts':'torrid-torch-3186')+".firebaseio.com/").constant('isDev',isDev);

dashboardApp.config(['$routeProvider',"$locationProvider",
    function($routeProvider) {
        $routeProvider.
            when('/signin', {
                templateUrl: 'views/signin.html',
                controller: 'authCtrl'
            }).
            when('/main', {
                templateUrl: 'views/main.html',
                controller: 'dashboardCtrl'
            }).
            when('/pushSender',{
                templateUrl: 'views/pushSender.html',
                controller: 'pushSender'
            }).
            otherwise({
                redirectTo: '/signin'
            }

        );
    }]);
