'use strict';
var tutors = [];
var not_tutors = [];
angular.module('dashboard.filters', [])
    .filter('unassigned_posts', function() {
        return function(posts) {
            var uposts = [];
            posts.forEach(function(value, key) {
                if (value.status == 'unanswered') {
                    uposts.push(value);
                }
            });
            return uposts;
        }
    })
    .filter('assigned_posts', function() {
        return function(posts) {
            var assigned_posts = [];
            posts.forEach(function(value, key) {
                if (value.status == 'assigned') {
                    assigned_posts.push(value);
                }
            });
            return assigned_posts;
        }
    })
    .filter('answered_posts', function() {
        return function(posts) {
            var answered_posts = [];
            posts.forEach(function(value, key) {
                if (value.status == 'answered') {
                    answered_posts.push(value);
                }
            });
            return answered_posts;
        }
    })
    .filter('count_unassigned', function() {
        return function(posts) {
            var unassigned = 0;
            posts.forEach(function(value, key) {
                if (value.status == 'unanswered') {
                    unassigned += 1;
                }
            });
            return unassigned;
        }
    })
    .filter('count_assigned', function() {
        return function(posts) {
            var assigned = 0;
            posts.forEach(function(value, key) {
                if (value.status == 'assigned') {
                    assigned += 1;
                }
            });
            return assigned;
        }
    })
    .filter('count_answered', function() {
        return function(posts) {
            var answered = 0;
            posts.forEach(function(value, key) {
                if (value.status == 'answered') {
                    answered += 1;
                }
            });
            return answered;
        }
    })
    .filter('isTutor', function() {
        return function(users) {
            tutors.length = 0;
            for(var user in users){
                if (users[user].is_teacher == true) {
                    tutors.push(users[user]);
                }
            }
            return tutors;
        }
    })
    .filter('isNotTutor', function() {
        return function(users) {
            not_tutors.length = 0;
            for(var user in users){
                if (users[user].is_teacher == false) {
                    not_tutors.push(users[user]);
                }
            };
            return not_tutors;
        }
    })
    .filter('startFrom', function() {
        return function(input, start) {
            if(input==null){
                return null;
            }
            start = +start; //parse to int
            return input.slice(start);
    }
});

;
