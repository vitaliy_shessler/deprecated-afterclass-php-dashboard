<?php
include_once "config.php";
include_once "db.php";
include_once "vendor/autoload.php";
date_default_timezone_set("UTC");


$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

if(isset($_REQUEST["action"])){
	$action=htmlspecialchars($_REQUEST["action"]);
}else{
	$action="help";
}
if($action=="setUserData") {
	if (empty($_REQUEST["user_id"]) == false && empty($_REQUEST["userData"]) == false) {
		$user_id = htmlspecialchars($_REQUEST["user_id"]);
		$user = $_REQUEST["userData"];

		$user = json_decode($user);
		if($user==null){
			echo("invalid user ".json_last_error_msg());
		}else {
			$firebase->set('/users/' . $user_id, $user);
		}

//		manageDB("INSERT INTO potential_tutors (post_id, user_id) VALUES ('$post_id', '$user_id')");
	} else {
		echo "send post_id and user_id";
	}
}else if($action=="getUserData") {
	if (empty($_REQUEST["user_id"]) == false) {
		$user_id = htmlspecialchars($_REQUEST["user_id"]);
		$user=$firebase->get('/users/' . $user_id);
		$json=json_encode($user);
		echo $user;
	} else {
		echo "send post_id and user_id";
	}
}else if($action=="getData"){
	$query='select name,user_id,is_teacher,profile_picture from users WHERE 1=1 ';

	if (empty($_REQUEST["pre"])==false) {
		$user_pre_fix = htmlspecialchars($_REQUEST["pre"]);
		$query .= ' AND `name` like \''.$user_pre_fix.'%\'';
	}
	if(empty($_REQUEST['is_teacher'])==false){
		$is_teacher=$_REQUEST['is_teacher']=='true'||$_REQUEST['is_teacher']=='on'||$_REQUEST['is_teacher']=='1';
		if(!$is_teacher){
			$is_teacher=false;
		}
		$query .= ' AND `is_teacher` = '.($is_teacher?"true":"false");
	}
	$query.=' LIMIT ';
	if(empty($_REQUEST['page'])==false && is_numeric($_REQUEST['page'])){
		$page_number=htmlspecialchars($_REQUEST['page']);
		$query .= $page_number.',';
	}
	$query.="10";
	//echo $query;
	$users = manageDB($query);

	$data=new \stdClass();
	$data->count=sizeof($users);
	$data->users=$users;
	echo json_encode($data);
}else if($action=="help"){
	?>!
	<form action="user.php?token=<?= $token?>" method="post">
		<input name="user_id" type="text" placeholder="user id"/>
		<input name="status" type="text" placeholder="status"/>
		<textarea name="userData" type="text" placeholder="userData (json like you get at getUserData)"></textarea>
		<input name="pre" type="text" placeholder="pre"/>
		<input name="is_teacher" type="text" placeholder="is_teacher, write 1 or 0/true or false/on or off"/>
		<select name="action">
			<option value="getData">getData</option>
			<option value="getUserData">getUserData</option>
			<option value="setUserData">setUserData</option>
		</select>
		<button type="submit">Send</button>
	</form>
	<?php
}else{
	echo "can't handle ".$action;
}
/*
SELECT
			p.post_id,
			p.body,
			p.status,
			p.subject,
			CONCAT('http://res.cloudinary.com/daayssulc/image/upload/c_fill,w_720/',p.img_id,'.jpg') as post_img_url,
			u.name,
			(SELECT GROUP_CONCAT(tutor.name, ',') FROM potential_tutors,users As tutor WHERE tutor.user_id=potential_tutors.user_id AND potential_tutors.post_id=p.post_id) AS tutors
FROM
			posts AS p,
			users AS u
WHERE
			p.user_id=u.user_id
 */

