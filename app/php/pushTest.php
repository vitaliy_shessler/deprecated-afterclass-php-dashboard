<?php
/**
 * Created by IntelliJ IDEA.
 * User: pc1
 * Date: 15/06/15
 * Time: 20:42
 */

// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyALRDeWzmOZ311Tjcqo7QzizeOZoFJcbdo');


$registrationIds = array(
	"APA91bEB2HeMw4LEkg8mtlB40rlDnxCmhQQMY_dIjG2qt_2_QHJT7GSevTAa17LQ-l68XnEbumdlh4fzt2woNJiirB8ENNlqVg6y7Brx3WzE_lpLoh3BJNF3A8Speh0GWI8eCPPlbijtmZYEQTdGD6mGGPbop_5PkQ"
);

// prep the bundle
$msg = array
(
	'message'       => 'here is a message. message',
	'title'         => 'This is a title. title',
	'subtitle'      => 'This is a subtitle. subtitle',
	'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
	'vibrate'   => 1,
	'sound'     => 1
);

$fields = array
(
	'registration_ids'  => $registrationIds,
	'data'              => $msg
);

$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);

$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );

echo $result;
