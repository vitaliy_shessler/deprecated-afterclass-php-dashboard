<?php
include_once "config.php";
include_once "db.php";

include_once "vendor/autoload.php";
date_default_timezone_set("UTC");


$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);


if(isset($_POST["action"])){
	$action=$_POST["action"];
}else{
	$action="help";
}
if($action=="addPostTutor") {
	if (empty($_POST["post_id"]) == false && empty($_POST["user_id"]) == false) {
		$user_id = htmlspecialchars($_POST["user_id"]);
		$post_id = htmlspecialchars($_POST["post_id"]);
		//first update firebase so i need to get the full post and full user
		$user = json_decode($firebase->get('/users/' . $user_id));
		$post = json_decode($firebase->get('/posts/' . $post_id));
		$post->potential_tutors->$user_id = $user;

		$total = count((array)$post->potential_tutors);
		if($total==0){
			$post->status="unanswered";
		}else{
			$post->status="assigned";
		}
		$post->update_date=time();
		$firebase->set('/posts/' . $post_id, $post);


		manageDB("INSERT INTO potential_tutors (post_id, user_id) VALUES ('$post_id', '$user_id')");
	} else {
		echo "send post_id and user_id";
	}
}else if($action=="rmPostTutor") {
	if (empty($_POST["post_id"]) == false && empty($_POST["user_id"]) == false) {
		$user_id = htmlspecialchars($_GET["user_id"]);
		$post_id = htmlspecialchars($_GET["post_id"]);

		$post = json_decode($firebase->get('/posts/' . $post_id));
		unset($post->potential_tutors->$user_id);
		$total = count((array)$post->potential_tutors);
		if($total==0){
			$post->status="unanswered";
		}else{
			$post->status="assigned";
		}
		$post->update_date=time();
		$firebase->set('/posts/' . $post_id, $post);
		manageDB("DELETE FROM potential_tutors WHERE post_id='$post_id' AND user_id='$user_id'");
	} else {
		echo "send post_id and user_id";
	}
}else if($action=="getData"){
	$query = 'SELECT * from `posts-view` WHERE 1=1';
	if (empty($_GET["status"]) == false) {
		$status = htmlspecialchars($_POST["status"]);
		$query .= ' AND `status` + \'' . $status . '%\'';
	}
	$query .= ' LIMIT 10';
	$users = manageDB($query);

	echo json_encode($users);
}else{
	?>
	<form action="post.php?token=<?= $token?>" method="post">
		<input name="post_id" type="text" placeholder="post id"/>
		<input name="status" type="text" placeholder="status""/>
		<input name="user_id" type="text" placeholder="user id"/>
		<select name="action">
			<option value="getData">getData</option>
			<option value="addPostTutor">addPostTutor</option>
			<option value="rmPostTutor">rmPostTutor</option>
		</select>
		<button type="submit">Send</button>
	</form>
<?php


}
/*
SELECT
			p.post_id,
			p.body,
			p.status,
			p.subject,
			CONCAT('http://res.cloudinary.com/daayssulc/image/upload/c_fill,w_720/',p.img_id,'.jpg') as post_img_url,
			u.name,
			(SELECT GROUP_CONCAT(tutor.name, ',') FROM potential_tutors,users As tutor WHERE tutor.user_id=potential_tutors.user_id AND potential_tutors.post_id=p.post_id) AS tutors
FROM
			posts AS p,
			users AS u
WHERE
			p.user_id=u.user_id
 */

